from aiohttp import web, websocket
import asyncio
import contextlib
import json
import logging
import random
import sys
import time
import requests

sys.setrecursionlimit(100000)
random.seed(451.738271)

class Card:
    def __init__(self, health, attack, cost, ID):
        self.health = health
        self.attack = attack
        self.cost = cost
        self.ID = ID

    def to_dict(self):
        return {'health': self.health, 'attack': self.attack, 'cost': self.cost, 'ID': self.ID}


class Player:
    def __init__(self, deck = [Card(2, 1, 1, 1), Card(2, 1, 1, 2)]):
        self.hash_code = ''
        self.deck = deck
        self.cards = []
        self.fieldCards = [None for i in range(6)]
        self.health = 10
        self.attack = 10
# While isWinner == None players can play
        self.isWinner = None
        self.turn = False
        self.msg_queue = asyncio.Queue()


class Battle:
    def __init__(self):
        self.turn = 1
        self.players = [None, None]
        self.finished = False
        self.winner = 0
        self.timeTurnEnds = 0
        self.turnStarted = False
        self.battleStarted = False


def to_dict(array):
    return [array[i].to_dict() for i in range(len(array))]

TURN_TIME = 30

battles = {}

loop = asyncio.get_event_loop()
log = logging.getLogger("game")


def check_player(hash_code=''):

    URL1 = 'http://b2wars.com/'
    URL2 = 'http://localhost:8000/'
    url_add = 'check_player_for_being_able_to_play'

    success = False
    try:
        client = requests.session()

        client.get(URL1)  # sets cookie
        csrftoken = client.cookies['csrftoken']

        params = {'code': hash_code, 'csrfmiddlewaretoken': csrftoken}
        r = client.post(
            URL1 + url_add, 
            data=params, 
            headers={'Referer': URL1 + url_add},
            cookies=dict(client.cookies))
        success = True
    except:
        pass

    if not success or r.status_code == 404:
        client = requests.session()
        client.get(URL2)  # sets cookie
        csrftoken = client.cookies['csrftoken']

        params = {'code': hash_code, 'csrfmiddlewaretoken': csrftoken}
        r = client.post(
            URL2 + url_add, 
            data=params, 
            headers={'Referer': URL2 + url_add},
            cookies=dict(client.cookies))

        if r.status_code == 404:
            return {'result': False}

    try:
        #log.info("RECEIVED: %s", r.text)
        data = r.text.split("|")

        if data[0].strip() == 'false':
            return {'result': False}
        else:
            data[1] = data[1].rstrip().lstrip().replace("&quot;", '"')
            log.info(repr(data[1]))
            check_result = {'result': True}
            check_result.update(json.loads(data[1]))
            return check_result
    except:
        return {'result': False}

def opposite_player(currentPlayer):
    return 1 - currentPlayer

def start_battle(battle):
    log.info("Battle started")
    battle.battleStarted = True
    for player in battle.players:
        if player.ID == 1:
            player.cards = [player.deck[0], player.deck[0], player.deck[1], player.deck[0], player.deck[1]];
        else:
            player.cards = [player.deck[1], player.deck[1], player.deck[1], player.deck[1], player.deck[0]];
        player.websocket.send_str("Start")

def send_command(ws, command):
    try:
        message = json.dumps(command)
        log.info("Sending: %s", message)
        ws.send_str(message)
    except:
        return

def execute_command(data, battleID, playerNumber):
    try:
        #log.info(str(data))
        command = data['command']
        player = battles[battleID].players[playerNumber]
        enemy = battles[battleID].players[opposite_player(playerNumber)]
        if (command == "LookingAtCard"):
            send_command(
                enemy.websocket, 
                {'EnemyLookingAtCard': True, 'card': data['card']})

        elif (command == "StopLookingAtCard"):
            send_command(
                enemy.websocket, 
                {'EnemyStopLookingAtCard': True, 'card': data['card']})

        elif (command == "MoveCardFromPlayerToField"):
            cardIndex = -1
            for i in range(len(player.cards)):
                if player.cards[i].ID == int(data['ID']):
                    cardIndex = i
                    break

            if cardIndex != -1:
                send_command(
                    player.websocket,
                    {'Verifying': True, 'Result': True})

                card = player.cards[cardIndex]
                player.fieldCards[int(data['to'])] = Card(card.health, card.attack, card.cost, card.ID)

                send_command(
                    enemy.websocket, 
                    {'MoveCardFromEnemyToField': True, 'card': data['card'], 
                    'cardInfo': {'ID': data['ID'], 'health': card.health, 'attack': card.attack}, 
                    'to': data['to']})

                del player.cards[cardIndex]

        elif (command == "Attack"):
            send_command(
                player.websocket,
                {'Verifying': True, 'Result': True})

            send_command(
                enemy.websocket, 
                {'EnemyAttack': True, 'card': data['card'], 'to': data['to']})

            playerCard = player.fieldCards[int(data['card'])]
            enemyCard = enemy.fieldCards[int(data['to'])]

            playerCard.health -= enemyCard.attack
            enemyCard.health -= playerCard.attack

            if player.fieldCards[int(data['card'])].health <= 0:
                player.fieldCards[int(data['card'])] = None

                send_command(
                    player.websocket,
                    {'MoveCardFromPlayerFieldToTrash': True, 'card': data['card']})
                send_command(
                    enemy.websocket,
                    {'MoveCardFromEnemyFieldToTrash': True, 'card': data['card']})

            if enemy.fieldCards[int(data['to'])].health <= 0:
                enemy.fieldCards[int(data['to'])] = None

                send_command(
                    player.websocket,
                    {'MoveCardFromEnemyFieldToTrash': True, 'card': data['to']})
                send_command(
                    enemy.websocket,
                    {'MoveCardFromPlayerFieldToTrash': True, 'card': data['to']})

        elif (command == "WallAttack"):
            send_command(
                player.websocket,
                {'Verifying': True, 'Result': True})

            send_command(
                enemy.websocket, 
                {'EnemyWallAttack': True, 'card': data['card']})

            playerCard = player.fieldCards[int(data['card'])]

            playerCard.health -= enemy.attack
            enemy.health -= playerCard.attack

            if player.fieldCards[int(data['card'])].health <= 0:
                player.fieldCards[int(data['card'])] = None

                send_command(
                    player.websocket,
                    {'MoveCardFromPlayerFieldToTrash': True, 'card': data['card']})
                send_command(
                    enemy.websocket,
                    {'MoveCardFromEnemyFieldToTrash': True, 'card': data['card']})

            if enemy.health <= 0:
                send_command(
                    player.websocket,
                    {'GameEnd': True, 'Winner': True})
                send_command(
                    enemy.websocket,
                    {'GameEnd': True, 'Winner': False})
                battles[battleID].finished = True
                battles[battleID].winner = playerNumber

        elif (command == "EndTurn"):
            battles[battleID].turn = opposite_player(playerNumber)
            battles[battleID].timeTurnEnds = loop.time() + TURN_TIME
            battles[battleID].turnStarted = False
            if enemy.websocket.closed:
                    battles[battleID].turnStarted = True

    except:
        return

def compose_info_message(battleID, currentPlayer):
    player = battles[battleID].players[currentPlayer]
    enemy = battles[battleID].players[opposite_player(currentPlayer)]
    msg = {
        'InfoMessage': True,
        'PlayerCards': to_dict(player.cards),
        'PlayerFieldCards': [None if card == None else card.to_dict() for card in player.fieldCards],
        'PlayerHealth': player.health,
        'PlayerTurn': player.turn,
        'EnemyCards': len(enemy.cards),
        'EnemyFieldCards': [None if card == None else card.to_dict() for card in enemy.fieldCards],
        'EnemyHealth': enemy.health
    }
 
    return msg

def take_card_from_deck(player, enemyWS, number):
    for i in range(number):
        card = random.choice(player.deck)
        player.cards += [card]
        try:
            send_command(
                player.websocket, 
                {'MoveCardFromDeckToPlayer': True, 'card': card.to_dict()})

            send_command(
                enemyWS, 
                {'MoveCardFromDeckToEnemy': True})
        except:
            return
    return

async def battle_handler(battleID, currentPlayer):
    player = battles[battleID].players[currentPlayer]
    enemy = battles[battleID].players[opposite_player(currentPlayer)]
    loop = asyncio.get_event_loop()

    try:
        send_command(player.websocket, compose_info_message(battleID, currentPlayer))
    except:
        pass


    log.info("%s started!", currentPlayer)
    while not battles[battleID].finished:

# Opponent's turn
        while battles[battleID].turn != currentPlayer and \
            not battles[battleID].finished:

            if enemy.websocket.closed and \
                loop.time() + 0.2 >= battles[battleID].timeTurnEnds: 

                battles[battleID].turn = currentPlayer
                battles[battleID].turnStarted = False
                battles[battleID].timeTurnEnds = loop.time() + TURN_TIME

                if not battles[battleID].finished:
                    take_card_from_deck(enemy, player.websocket, 1)
            else:
                next_msg_future = player.msg_queue.get()
                try:
                    msg = await asyncio.wait_for(next_msg_future, timeout=0.5)
                except asyncio.TimeoutError:
                    pass

# Setting up Player's turn
        if not battles[battleID].finished and \
            not player.websocket.closed and \
            not battles[battleID].turnStarted:

            try:
                send_command(player.websocket, {'StartTurn': True, 'Time': TURN_TIME})
            except:
                pass
            log.info("{} Player's turn!".format(currentPlayer))
            battles[battleID].turnStarted = True

        elif not battles[battleID].finished and \
            not player.websocket.closed and \
            battles[battleID].turnStarted:

            try:
                send_command(player.websocket, \
                    {'StartTurn': True, \
                    'Time': round(battles[battleID].timeTurnEnds - loop.time())})
            except:
                pass
            log.info("{} Player's turn continues!".format(currentPlayer))

# Player's turn
        while battles[battleID].turn == currentPlayer and \
            not battles[battleID].finished:

            if loop.time() + 0.2 >= battles[battleID].timeTurnEnds: 
                send_command(player.websocket, {'EndTurn': True})

                battles[battleID].turn = opposite_player(currentPlayer)
                battles[battleID].turnStarted = False
                battles[battleID].timeTurnEnds = loop.time() + TURN_TIME
                if enemy.websocket.closed:
                    battles[battleID].turnStarted = True
            else:
                next_msg_future = player.msg_queue.get()
                try:
                    msg = await asyncio.wait_for(next_msg_future, timeout=0.5)
                    execute_command(msg, battleID, currentPlayer)
                except asyncio.TimeoutError:
                    pass

# End of turn: new card for player
        if not battles[battleID].finished:
            take_card_from_deck(player, enemy.websocket, 1)


    log.info("Handler Finishing")
    try:
        await player.websocket.close()
    except:
        pass
    battles[battleID].finished = False
    return


async def ws_msg_collector(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    hash_code = request.match_info['hash_code']
    can_play = check_player(hash_code)
    if not can_play['result']:
        send_command(
            ws, 
            {'Banned': True})
        await ws.close()
        return ws
    else:
        battleID = can_play['battle']

    player = -1
    battleStarted = False

    if battleID not in battles:
        battles[battleID] = Battle()
        battles[battleID].finished = False
        battles[battleID].turn = 0

    if battles[battleID].players[0] == None:
        player = 0
        battles[battleID].players[player] = Player()
        battles[battleID].players[player].hash_code = hash_code
        battles[battleID].players[player].websocket = ws

    elif battles[battleID].players[0].hash_code == hash_code:
        player = 0
        await asyncio.sleep(1)
        battles[battleID].players[player].websocket = ws
        
        if battles[battleID].battleStarted:
            battleStarted = True
            battles[battleID].players[player].msg_queue = asyncio.Queue()
            loop = asyncio.get_event_loop()
            task = [asyncio.ensure_future(battle_handler(battleID, player))]

    elif battles[battleID].players[1] == None:
        player = 1
        battles[battleID].players[player] = Player()
        battles[battleID].players[player].hash_code = hash_code
        battles[battleID].players[player].websocket = ws

        start_battle(battles[battleID])
    elif battles[battleID].players[1].hash_code == hash_code:
        player = 1
        await asyncio.sleep(1)
        battles[battleID].players[player].websocket = ws

        if battles[battleID].battleStarted:
            battleStarted = True
            battles[battleID].players[player].msg_queue = asyncio.Queue()
            loop = asyncio.get_event_loop()
            task = [asyncio.ensure_future(battle_handler(battleID, player))]

    else:
        send(
            ws, 
            {'Banned': True})
        await ws.close()
        return ws


    async for msg in ws:
        if msg.tp == websocket.MSG_TEXT:
            log.info("Received: %s", str(msg))

            if msg.data == "Starting" and\
                battles[battleID].players[0] != None and \
                battles[battleID].players[1] != None:

                loop = asyncio.get_event_loop()

                battleStarted = True
                battles[battleID].battleStarted = True;
                battles[battleID].timeTurnEnds = loop.time() + TURN_TIME
                task = [asyncio.ensure_future(battle_handler(battleID, player))]

            elif msg.data == "close":
                await ws.close()
            elif battleStarted:
                await battles[battleID].players[player].msg_queue.put(json.loads(msg.data))

        elif msg.tp == websocket.MSG_CLOSE:
            log.info("Error: %s", str(msg))
            break
        else:
            log.info("Strange msg: %s", str(msg))  

    if battles[battleID].finished:
        result = await asyncio.wait(task)
    elif battleStarted:
        task[0].cancel()
        log.info("cancelled!")

    log.info("Closing websocket connection.")
    return ws

    
async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/battle/{hash_code}", ws_msg_collector)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)

    return server


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()

