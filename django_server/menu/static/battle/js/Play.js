jQuery(document).ready(function($) {

            var playerID = 1;
            var infoMessage = true;
            var toVerify = null;
            var MaxHealth = 10;
            var MaxDistance = 50;
            var TurnTime = 30;
            var Free = true;

            var EnemyCards = [];
            var EnemyFieldCards = [];
            var EnemyPositions = [];
            var PlayerCards = [];
            var PlayerFieldCards = [];
            var PlayerPositions = [];

            var turn = true;
            var timer = null;
            var cardScale = 0.5;
            var cardBigScale = 0.7;
            var cardEnemyY = 80;
            var cardEnemyLeftX = 212;
            var cardEnemyRightX = 805;
            var cardPlayerY = 637;
            var cardPlayerLeftX = 212;
            var cardPlayerRightX = 805;
            var cardWidth = 192;
            var cardHeight = 284;

            var EnemyWall = {
                'x1': 205, 
                'y1': 167,
                'x2': 811,
                'y2': 210
            };
            var PlayerWall = {
                'x1': 205,
                'y1': 520,
                'x2': 811,
                'y2': 552
            };

            var socket = new WebSocket("ws://" + window.location.hostname + ":80/battle/" + hash_code);

            var SendInfoMessage = function() {
                var message = {
                    player: playerID
                }
                socket.send(JSON.stringify(message));
            };

            socket.onmessage = function(event) {
                console.log(event.data);
                ParseMessage(event.data);
            };

            function ParseMessage(data) {
                if (data == "Start") {
                    textWaitingForOpponent.visible = false;
                    textOpponentsTurn.visible = true;
                    socket.send("Starting");
                    SendInfoMessage();
                    return;                    
                }

                var data = JSON.parse(data);
                if (data.InfoMessage) {
                    UpdatePlayers(data);
                }
                if (data.Verifying) {
                    toVerify = data.Result;
                }
                if (data.StartTurn) {
                    StartTurn(data.Time);
                }
                if (data.EndTurn) {
                    EndTurn();
                }

                if (data.EnemyLookingAtCard) {
                    EnemyLookingAtCard(data.card);
                }
                if (data.EnemyStopLookingAtCard) {
                    EnemyStopLookingAtCard(data.card);
                }

                if (data.MoveCardFromEnemyToField) {
                    MoveFromEnemyToField(data.card, data.cardInfo, data.to);
                }
                if (data.MoveCardFromPlayerToField) {
                    MoveFromPlayerToField(data.card, data.to);
                }

                if (data.MoveCardFromDeckToEnemy) {
                    MoveFromDeckToEnemy();
                }
                if (data.MoveCardFromDeckToPlayer) {
                    MoveFromDeckToPlayer(data.card);
                }

                if (data.MoveCardFromEnemyFieldToTrash) {
                    MoveFromEnemyFieldToTrash(data.card);
                }
                if (data.MoveCardFromPlayerFieldToTrash) {
                    MoveFromPlayerFieldToTrash(data.card);
                }

                if (data.EnemyAttack) {
                    var card = EnemyFieldCards[data.card];
                    var enemyCard = PlayerFieldCards[data.to];
                    Attack(
                        card,
                        enemyCard,
                        {
                            'x': PlayerPositions[data.to].x, 
                            'y': PlayerPositions[data.to].y + 50
                        });
                }
                if (data.PlayerAttack) {
                    var card = PlayerFieldCards[data.card];
                    var enemyCard = EnemyFieldCards[data.to];
                    Attack(
                        card,
                        enemyCard,
                        {
                            'x': card.position.x, 
                            'y': card.position.y - 50
                        });
                }

                if (data.EnemyWallAttack) {
                    var card = EnemyFieldCards[data.card];
                    AttackWall(
                        card,
                        "PlayerWall"
                    );

                }
            };

//---------- Cards & Stage--------

            function CreateCard(path, x, y, cardScale, player, enemy, field) {
                var card = new PIXI.Sprite.fromImage(path);
                card.position.x = x;
                card.position.y = y;
                card.anchor.x = 0.5;
                card.anchor.y = 0.5;
                card.scale.x = cardScale;
                card.scale.y = cardScale;

                card.card = true;
                card.enemy = enemy;
                card.field = field;
                card.player = player;

                card.previousPosition = {
                    'x': card.position.x,
                    'y': card.position.y
                };

                if (card.player || card.field) {
                    var TextStyle = {
                        font: '24px Arial',
                        align: 'center',
                        fill: '#FFFFFF',
                        stroke: '#000000',
                        strokeThickness: 4,
                    }

                    var health = new PIXI.Text("0", TextStyle);
                    health.anchor.set(0.5);
                    health.x = -cardWidth/2 + 20; 
                    health.y = -cardHeight/2 + 15;

                    card.addChild(health);

                    var attack = new PIXI.Text("0", TextStyle);
                    attack.anchor.set(0.5);
                    attack.x = cardWidth/2 - 20;
                    attack.y = -cardHeight/2 + 15;

                    card.addChild(attack);

                    card.attack = attack;
                    card.health = health;
                }

                return card;
            }

            function Interactivity(card) {
                card.buttonMode = true;
                card.interactive = true;
                card
                    // set the mousedown and touchstart callback
                    .on('mousedown', OnButtonDown)
                    .on('touchstart', OnButtonDown)

                    // set the mouseup and touchend callback
                    .on('mouseup', OnButtonUp)
                    .on('touchend', OnButtonUp)
                    .on('mouseupoutside', OnButtonUp)
                    .on('touchendoutside', OnButtonUp)

                    // set the drag move callback
                    .on('mousemove', onDragMove)
                    .on('touchmove', onDragMove)

                    // set the mouseover callback
                    .on('mouseover', OnButtonOver)

                    // set the mouseout callback
                    .on('mouseout', OnButtonOut);
            }

            var renderer = new PIXI.autoDetectRenderer(1000, 720, {
                'antialias': true,
                resolution: window.devicePixelRatio
            });
            document.body.appendChild(renderer.view);

            var stage = new PIXI.Container();
            var background = PIXI.Sprite.fromImage(backgroundImage);
            background.width = renderer.width;
            background.height = renderer.height;
            stage.addChild(background);
            renderer.render(stage);

            var deck = CreateCard(
                cardBackImage,
                940, 640,
                cardScale,
                player=false,
                enemy=false,
                field=false
            );
            var enemyDeck = CreateCard(
                cardBackImage,
                60, 80,
                cardScale,
                player=false,
                enemy=false,
                field=false
            );
            stage.addChild(deck);
            stage.addChild(enemyDeck);

            var trash = {
                'x': 60,
                'y': 280
            }

            for (var i = 0; i < 6; i++) {
                EnemyPositions[i] = {
                    'x': 155 + 101 * (i + 1),
                    'y': 285
                };
                PlayerPositions[i] = {
                    'x': 155 + 101 * (i + 1),
                    'y': 435
                };
            }

//---------- HEALTHBARS ----------

            function CreateHealthBar(x, y) {
                var healthBar = new PIXI.Container();
                healthBar.position.set(x, y);
                stage.addChild(healthBar);

                var innerBar = new PIXI.Graphics();
                innerBar.beginFill(0x000000);
                innerBar.drawRect(0, 0, 102, 10);
                innerBar.endFill();
                healthBar.addChild(innerBar);

                var outerBar = new PIXI.Graphics();
                outerBar.beginFill(0xFF3300);
                outerBar.drawRect(1, 1, 100, 8);
                outerBar.endFill();
                healthBar.addChild(outerBar);

                healthBar.outer = outerBar;

                return healthBar;
            }

            function UpdateHealth(healthBar, newValue) {
                healthBar.outer.width = newValue / MaxHealth * 100;
            }

            function GetHealth(healthBar) {
                return healthBar.outer.width / 100 * MaxHealth;
            }

            PlayerHealthBar = CreateHealthBar(83, stage.height - 31);
            EnemyHealthBar = CreateHealthBar(stage.width - 168, 132);

//---------- Texts ----------

            function CreateText(msg, style, x, y) {
                var text = new PIXI.Text(msg, style);
                text.anchor.set(0.5);
                text.x = x;
                text.y = y;
                stage.addChild(text);

                return text;
            }

            var buttonTextStyle = {
                font: '26px Arial',
                align: 'center',
                fill: '#000000',
                //stroke: '#000000',
                //strokeThickness: 4,
                wordWrap: 'true',
                wordWrapWidth:'80'
            }
            var screenTextStyle = {
                font: '20px Arial',
                align: 'left',
                fill: '#000000',
                stroke: '#ffffff',
                strokeThickness: 4,
                wordWrap: 'true',
                wordWrapWidth:'160'
            }

            var textPlayersTurn = CreateText(
                "It's your turn now.", 
                screenTextStyle, 
                stage.width - 100, 
                350);
            textPlayersTurn.visible = false;

            var textOpponentsTurn = CreateText(
                "Waiting for opponent's turn.", 
                screenTextStyle, 
                stage.width - 100, 
                350);
            textOpponentsTurn.visible = false;

            var textWaitingForOpponent = CreateText(
                "Waiting for opponent.", 
                screenTextStyle, 
                stage.width - 100, 
                350);
            textWaitingForOpponent.visible = true;

            var textTimer = CreateText(
                "", 
                screenTextStyle, 
                942, 
                482);
            textTimer.visible = false;

//---------- Buttons ----------

            function CreateButton(x, y) {
                var button = new PIXI.Container();
                button.position.set(x, y);
                stage.addChild(button);

                var toPress = new PIXI.Graphics();
                //toPress.beginFill(0xb1b8c7);
                toPress.beginFill(0xFFFFFF);
                toPress.drawRect(0, 0, 80, 60);
                toPress.endFill();
                button.addChild(toPress);

                var text = new PIXI.Text('End Turn', buttonTextStyle);
                text.anchor.set(0.5);
                text.x = 40;
                text.y = 30;
                button.addChild(text);

                ButtonInteractivity(button);

                return button;
            }

            function ButtonInteractivity(button) {
                button.buttonMode = true;
                button.interactive = true;
                button.click = Noop;
            }

            function Noop(event) {
                msg = {
                    command: "EndTurn"
                }
                console.log("Sending: " + JSON.stringify(msg));
                socket.send(JSON.stringify(msg));
                EndTurn();                
            }

            buttonEndTurn = CreateButton(900, 500);
            buttonEndTurn.visible = false;

//--------------------------------------

            animate();
            function animate() {
                renderer.render(stage);
                TWEEN.update();
                requestAnimationFrame(animate);
            }

            function OnButtonDown(event) {
                if (Free) {
                    if (this.card && this.player && !this.field) {
                        Free = false;
                        this.dragging = true;
                        this.toField = true;
                        this.data = event.data;
                        this.alpha = 0.5;
                    } else if (this.card && this.player && this.field) {
                        Free = false;
                        this.toAttack = true;
                        this.alpha = 0.5;
                    }
                }
                return;
            }

            function OnButtonUp() {
                console.log("ButtonUp");
                toBeVerified = false;
                // Move card to the field
                if (this.card && this.player && !this.field && this.dragging) {
                    this.alpha = 1;
                    var moved = false;
                    this.dragging = false;

                    if (toVerify != null) {
                        OnButtonUpEnd(false, this, null);
                        toBeVerified = true;
                    } else {
                        var curPosition = this.data.getLocalPosition(this.parent);
                        for (var i = 0; i < PlayerPositions.length; i++) {
                            if (PlayerFieldCards[i] == null && 
                                Distance(curPosition, PlayerPositions[i]) < MaxDistance) {

                                MoveFromPlayerToField(this.index, i, 100);
                                moved = true;
                                toBeVerified = true;

                                break;
                            }
                        }
                        this.data = null;

                        if (!moved) {
                            OnButtonUpEnd(false, this, i);
                            toBeVerified = true;
                        }
                    }
                // Attack

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//To improve: Attack by pressing on enemy card, do not calculate distance
                } else if (this.card && this.player && this.field && this.attack) {
                    this.alpha = 1;

                    var curPosition = renderer.plugins.interaction.mouse.global;

                    if (toVerify != null) {
                        this.toAttack = false;
                    } else if (IsInside(curPosition, EnemyWall)) { 
                        console.log("It's wall!");
                        this.toAttackWall = true;
                        this.toAttack = false;
                        SendVerifying("WallAttack", this, null);
                        toBeVerified = true;
                    } else {
                        for (var i = 0; i < EnemyPositions.length; i++) {
                            if (EnemyFieldCards[i] != null && 
                                Distance(curPosition, EnemyPositions[i]) < MaxDistance) {

                                EnemyFieldCards[i].scale.x = cardScale;
                                EnemyFieldCards[i].scale.y = cardScale;
                                SendVerifying("Attack", this, i);
                                toBeVerified = true;

                                break;
                            }
                        }
                    }
                }
                if (!toBeVerified) {
                    Free = true;
                } 
            }

            // Function is called by SendVerifying function, if player's action
            // was accepted be server
            function OnButtonUpEnd(success, card, to) {
                if (card.toField) {
                    this.toField = false;
                    if (!success) {
                        var move_card = new TWEEN.Tween(card.position)
                            .to({'x': card.previousPosition.x, 'y': card.previousPosition.y}, 300)
                            .easing(TWEEN.Easing.Quadratic.InOut);

                        move_card.start();
                    } else {
                        MoveFromPlayerToFieldEnd(card.index, to);
                    }
                } else if (card.toAttack) {
                    card.toAttack = false;

                    if (success) {
                        Attack(
                            card,
                            EnemyFieldCards[to],
                            {
                                'x': EnemyPositions[to].x, 
                                'y': EnemyPositions[to].y + 50
                            });
                    }
                } else if (card.toAttackWall) {
                    card.toAttackWall = false;

                    if (success) {
                        AttackWall(
                            card,
                            "EnemyWall"
                        );
                    }
                }
                Free = true;
            } 

            function OnButtonOver() {
                if (!this.enemy || (this.enemy && this.field)) {
                    this.scale.x = cardBigScale;
                    this.scale.y = cardBigScale;

                    if (this.player && !this.field) {
                        var message = {
                            command: "LookingAtCard",
                            card: this.index
                        }
                        console.log("Sending: " + JSON.stringify(message));
                        socket.send(JSON.stringify(message));
                    }
                }

            }

            function OnButtonOut()
            {
                if (!this.enemy || (this.enemy && this.field)) {
                    this.scale.x = cardScale;
                    this.scale.y = cardScale;

                    if (this.player && !this.field) {
                        var message = {
                            command: "StopLookingAtCard",
                            card: this.index
                        }
                        console.log("Sending: " + JSON.stringify(message));
                        socket.send(JSON.stringify(message));
                    }
                }
            }

            function onDragMove() {
                if (this.dragging) {
                    var newPosition = this.data.getLocalPosition(this.parent);
                    this.position.x = newPosition.x;
                    this.position.y = newPosition.y;
                }
            }

//----- COMMON utilities -----
            function StartTurn(time) {
                turn = true;
                buttonEndTurn.visible = true;
                textOpponentsTurn.visible = false;
                textPlayersTurn.visible = true;

                textTimer.visible = true;
                textTimer.text = time;
                timer = setInterval(function(){
                    textTimer.text = parseInt(textTimer.text) - 1;
                    }, 1000);
            }

            function EndTurn() {
                turn = false;
                buttonEndTurn.visible = false;
                textPlayersTurn.visible = false;
                textOpponentsTurn.visible = true;

                if (timer != null) {
                    textTimer.visible = false;
                    clearInterval(timer);
                }
            }

            function UpdateCardsPositions(cards, leftX, rightX, Y) {
                var widthConstant = 2/3;
                var width = cardWidth * cardScale;
                if (cards.length >= (rightX - leftX - width) / (widthConstant * width)) {
                    var dist = (rightX - leftX - width) / (cards.length - 1);
                    for (var i = 0; i < cards.length; i++) {
                        cards[i].position.x = 
                            leftX + 
                            i * width +
                            width / 2 -
                            i * (width - dist);
                        cards[i].position.y = Y;
                        cards[i].previousPosition.x = cards[i].position.x;
                        cards[i].previousPosition.y = cards[i].position.y;
                    }
                } else {
                    var dist = width * widthConstant;
                    var totalSize = cards.length*width - (cards.length-1) * (width-dist);
                    var leftBound = leftX + (rightX - leftX - totalSize) / 2;
                    for (var i = 0; i < cards.length; i++) {
                        cards[i].position.x = 
                            leftBound + 
                            i * width +
                            width / 2 -
                            i * (width - dist);
                        cards[i].position.y = Y;
                        cards[i].previousPosition.x = cards[i].position.x;
                        cards[i].previousPosition.y = cards[i].position.y;
                    }
                }
            }

            function UpdatePlayers(data) {
                textWaitingForOpponent.visible = false;
                textOpponentsTurn.visible = true;

                for (var i = 0; i < PlayerCards.length; i++) {
                    stage.removeChild(PlayerCards[i]);
                    delete PlayerCards[i];
                }
                for (var i = 0; i < data.PlayerCards.length; i++) {
                    PlayerCards[i] = CreateCard(
                        cardStorage + data.PlayerCards[i].ID + cardFormat, 
                        0, 0, 
                        cardScale,
                        player=true,
                        enemy=false,
                        field=false
                    );
                    Interactivity(PlayerCards[i]);
                    PlayerCards[i].index = i;
                    PlayerCards[i].ID = data.PlayerCards[i].ID;
                    PlayerCards[i].attack.text = data.PlayerCards[i].attack;
                    PlayerCards[i].health.text = data.PlayerCards[i].health;

                    stage.addChild(PlayerCards[i]);
                }
                UpdateCardsPositions(PlayerCards, cardPlayerLeftX, cardPlayerRightX, cardPlayerY);
                UpdateHealth(PlayerHealthBar, data.PlayerHealth);

                for (var i = 0; i < data.PlayerFieldCards.length; i++) {
                    if (data.PlayerFieldCards[i] == null &&
                        PlayerFieldCards[i] != null) {

                        stage.removeChild(PlayerFieldCards[i]);
                        delete PlayerFieldCards[i];
                        PlayerFieldCards[i] = null;
                    } else if (data.PlayerFieldCards[i] != null) {
                        PlayerFieldCards[i] = CreateCard(
                            cardStorage + data.PlayerFieldCards[i].ID + cardFormat, 
                            PlayerPositions[i].x, 
                            PlayerPositions[i].y, 
                            cardScale,
                            player=true,
                            enemy=false,
                            field=true
                        );
                        Interactivity(PlayerFieldCards[i]);
                        PlayerFieldCards[i].index = i;
                        PlayerFieldCards[i].ID = data.PlayerFieldCards[i].ID;
                        PlayerFieldCards[i].attack.text = data.PlayerFieldCards[i].attack;
                        PlayerFieldCards[i].health.text = data.PlayerFieldCards[i].health;

                        stage.addChild(PlayerFieldCards[i]);
                    }
                }

                for (var i = 0; i < EnemyCards.length; i++) {
                    stage.removeChild(EnemyCards[i]);
                    delete EnemyCards[i];
                }
                for (var i = 0; i < data.EnemyCards; i++) {
                    EnemyCards[i] = CreateCard(
                        cardBackImage, 
                        0, 0,
                        cardScale,
                        player=false,
                        enemy=true,
                        field=false
                    );
                    Interactivity(EnemyCards[i]);
                    EnemyCards[i].index = i;

                    stage.addChild(EnemyCards[i]);
                }
                UpdateCardsPositions(EnemyCards, cardEnemyLeftX, cardEnemyRightX, cardEnemyY); 
                UpdateHealth(EnemyHealthBar, data.EnemyHealth);

                for (var i = 0; i < data.EnemyFieldCards.length; i++) {
                    if (data.EnemyFieldCards[i] == null &&
                        EnemyFieldCards[i] != null) {

                        stage.removeChild(EnemyFieldCards[i]);
                        delete EnemyFieldCards[i];
                        EnemyFieldCards[i] = null;
                    } else if (data.EnemyFieldCards[i] != null) {
                        EnemyFieldCards[i] = CreateCard(
                            cardStorage + data.EnemyFieldCards[i].ID + cardFormat, 
                            EnemyPositions[i].x,
                            EnemyPositions[i].y, 
                            cardScale,
                            player=false,
                            enemy=true,
                            field=true
                        );
                        Interactivity(EnemyFieldCards[i]);
                        EnemyFieldCards[i].index = i;
                        EnemyFieldCards[i].ID = data.EnemyFieldCards[i].ID;
                        EnemyFieldCards[i].attack.text = data.EnemyFieldCards[i].attack;
                        EnemyFieldCards[i].health.text = data.EnemyFieldCards[i].health;

                        stage.addChild(EnemyFieldCards[i]);
                    }
                }
            }

            function SendVerifying(command, card, to) {
                toVerify = false;
                var msg = {
                    "command": command,
                    "card": card.index,
                    "ID": card.ID,
                    "to": to
                };

                console.log("Sending: " + JSON.stringify(msg));
                socket.send(JSON.stringify(msg));

                WaitVerifying(0, function() {

                    if (command == "MoveCardFromPlayerToField") {
                        if (toVerify) {
                            OnButtonUpEnd(true, PlayerCards[card.index], to);
                        } else {
                            OnButtonUpEnd(false, PlayerCards[card.index], to);
                        }
                    } else if (command == "Attack") {
                        if (toVerify) {
                            OnButtonUpEnd(true, PlayerFieldCards[card.index], to);
                        } else {
                            OnButtonUpEnd(false, PlayerFieldCards[card.index], to);
                        }
                    } else if (command == "WallAttack") {
                        if (toVerify) {
                            OnButtonUpEnd(true, PlayerFieldCards[card.index], null);
                        } else {
                            OnButtonUpEnd(false, PlayerFieldCards[card.index], null);
                        }

                    }
                    toVerify = null;
                });
            }

            function WaitVerifying(depth, callback) {
                console.log("Waiting ", depth);
                setTimeout(
                    function() {
                        if (depth < 100 && toVerify == false) {
                            depth++;
                            WaitVerifying(depth, callback);
                        } else {
                            callback();
                        }
                    }, 5);      
            }

            function DeleteCard(cardArray, cardNumber, player) {
                for (var i = 0; i < cardArray.length - 1; i++) {
                    if (i >= cardNumber) {
                        cardArray[i] = cardArray[i + 1];
                        cardArray[i].index = i;
                    }
                }
                cardArray.splice(cardArray.length - 1, cardArray.length);
                if (player == "player") {
                    UpdateCardsPositions(PlayerCards, cardPlayerLeftX, cardPlayerRightX, cardPlayerY);
                } else if (player == "enemy") {
                    UpdateCardsPositions(EnemyCards, cardEnemyLeftX, cardEnemyRightX, cardEnemyY);
                }
            }

            function IsInside(point, rectangle) {
                if (point.x >= Math.min(rectangle.x1, rectangle.x2) &&
                    point.x <= Math.max(rectangle.x1, rectangle.x2) &&
                    point.y >= Math.min(rectangle.y1, rectangle.y2) &&
                    point.y <= Math.max(rectangle.y1, rectangle.y2)) {
                    return true;
                } else {
                    return false;
                }
            }

            function Distance(position1, position2) {
                return Math.pow(
                    Math.pow(position1.x - position2.x, 2) + 
                    Math.pow(position1.y - position2.y, 2), 
                    0.5);
            }

//----- ENEMY's animation -----
            function EnemyLookingAtCard(cardNumber) {
                EnemyCards[cardNumber].scale.x = cardBigScale;
                EnemyCards[cardNumber].scale.y = cardBigScale;
            }

            function EnemyStopLookingAtCard(cardNumber) {
                EnemyCards[cardNumber].scale.x = cardScale;
                EnemyCards[cardNumber].scale.y = cardScale;
            }

            function MoveFromDeckToEnemy() {
                var cardNumber = EnemyCards.length;

                var card = CreateCard(
                    cardBackImage,
                    enemyDeck.position.x,
                    enemyDeck.position.y,
                    cardScale,
                    player=false,
                    enemy=true,
                    field=false
                );
                Interactivity(card);
                card.index = cardNumber;

                stage.addChild(card);

                var move_card = new TWEEN.Tween(card.position)
                        .to({
                        'x': cardEnemyLeftX + (cardEnemyRightX - cardEnemyLeftX)/2, 
                        'y': cardEnemyY
                        }, 1000)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();

                EnemyCards[cardNumber] = card;
                setTimeout(function(){UpdateCardsPositions(EnemyCards, cardEnemyLeftX, cardEnemyRightX, cardEnemyY);}, 1050);
            }

            function MoveFromEnemyToField(cardNumber, cardInfo, newPosition) {
                var move_card = new TWEEN.Tween(EnemyCards[cardNumber].position)
                        .to({
                            'x': EnemyPositions[newPosition].x, 
                            'y': EnemyPositions[newPosition].y
                            }, 1000)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();
                EnemyFieldCards[newPosition] = CreateCard(
                    cardStorage + cardInfo.ID + cardFormat,
                    EnemyPositions[newPosition].x,
                    EnemyPositions[newPosition].y,
                    cardScale,
                    player=false,
                    enemy=true,
                    field=true
                );
                Interactivity(EnemyFieldCards[newPosition]);
                EnemyFieldCards[newPosition].ID = cardInfo.ID;
                EnemyFieldCards[newPosition].attack.text = cardInfo.attack;
                EnemyFieldCards[newPosition].health.text = cardInfo.health;

                setTimeout(function(){stage.addChild(EnemyFieldCards[newPosition]);}, 1000);
                setTimeout(function(){stage.removeChild(EnemyCards[cardNumber]);}, 1000);
                setTimeout(function(){DeleteCard(EnemyCards, cardNumber, "enemy");}, 1000);

                setTimeout(function(){UpdateCardsPositions(EnemyCards, cardEnemyLeftX, cardEnemyRightX, cardEnemyY);}, 1050);

            }

            function MoveFromEnemyFieldToTrash(cardPosition) {
                setTimeout(function() {
                    var move_card = new TWEEN.Tween(EnemyFieldCards[cardPosition].position)
                            .to({
                                'x': trash.x, 
                                'y': trash.y
                                }, 1000)
                            .easing(TWEEN.Easing.Quadratic.InOut);

                    move_card.start();
                    setTimeout(function(){stage.removeChild(EnemyFieldCards[cardPosition]);}, 1000);
                    setTimeout(function(){EnemyFieldCards[cardPosition] = null;}, 1010);
                }, 500);
            }

//----- PLAYER's animation -----

            function MoveFromDeckToPlayer(cardInfo) {
                var cardNumber = PlayerCards.length;

                var card = CreateCard(
                    cardStorage + cardInfo.ID + cardFormat,
                    deck.position.x,
                    deck.position.y,
                    cardScale,
                    player=true,
                    enemy=false,
                    field=false
                );
                Interactivity(card);
                card.index = cardNumber;
                card.ID = cardInfo.ID;
                card.health.text = cardInfo.health;
                card.attack.text = cardInfo.attack;

                stage.addChild(card);

                var move_card = new TWEEN.Tween(card.position)
                        .to({
                        'x': cardPlayerLeftX + (cardPlayerRightX - cardPlayerLeftX)/2, 
                        'y': cardPlayerY
                        }, 1000)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();

                PlayerCards[cardNumber] = card;
                setTimeout(function(){UpdateCardsPositions(PlayerCards, cardPlayerLeftX, cardPlayerRightX, cardPlayerY);}, 1050);
            }

            function MoveFromPlayerToField(cardNumber, newPosition, speed) {
                var move_card = new TWEEN.Tween(PlayerCards[cardNumber].position)
                        .to({
                            'x': PlayerPositions[newPosition].x, 
                            'y': PlayerPositions[newPosition].y
                            }, speed)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();

                SendVerifying("MoveCardFromPlayerToField", PlayerCards[cardNumber], newPosition);
            }

            function MoveFromPlayerToFieldEnd(cardNumber, newPosition) {
                PlayerFieldCards[newPosition] = CreateCard(
                    cardStorage + PlayerCards[cardNumber].ID + cardFormat,
                    PlayerPositions[newPosition].x,
                    PlayerPositions[newPosition].y,
                    cardScale,
                    player=true,
                    enemy=false,
                    field=true
                );
                Interactivity(PlayerFieldCards[newPosition]);
                PlayerFieldCards[newPosition].index = newPosition;
                PlayerFieldCards[newPosition].attack.text = PlayerCards[cardNumber].attack.text;
                PlayerFieldCards[newPosition].health.text = PlayerCards[cardNumber].health.text;

                setTimeout(function(){stage.addChild(PlayerFieldCards[newPosition]);}, 1000);
                setTimeout(function(){stage.removeChild(PlayerCards[cardNumber]);}, 1000);
                setTimeout(function(){DeleteCard(PlayerCards, cardNumber, "player");}, 1000);
            } 

            function MoveFromPlayerFieldToTrash(cardPosition) {
                setTimeout(function() {
                    var move_card = new TWEEN.Tween(PlayerFieldCards[cardPosition].position)
                            .to({
                                'x': trash.x, 
                                'y': trash.y
                                }, 1000)
                            .easing(TWEEN.Easing.Quadratic.InOut);

                    move_card.start(); 
                    setTimeout(function(){stage.removeChild(PlayerFieldCards[cardPosition]);}, 1000);
                    setTimeout(function(){PlayerFieldCards[cardPosition] = null;}, 1010);
                }, 500);
            }

//----- COMMON animation -----

            function Attack(card, enemyCard, position_to) {
                var position_from = card.position;

                var move_card = new TWEEN.Tween(card.position)
                        .to({
                            'x': position_to.x, 
                            'y': position_to.y
                            }, 200)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();

                enemyCard.health.text = 
                    parseInt(enemyCard.health.text, 10) - parseInt(card.attack.text, 10);
                card.health.text = 
                    parseInt(card.health.text, 10) - parseInt(enemyCard.attack.text, 10);


                var move_card_back = new TWEEN.Tween(card.position)
                        .to({
                            'x': position_from.x, 
                            'y': position_from.y
                            }, 200)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                setTimeout(function(){move_card_back.start();}, 190);
            }

            function AttackWall(card, mode) {
                var position_from = card.position;

                if (mode == "PlayerWall") {
                    HealthBar = PlayerHealthBar;
                    wall = PlayerWall;
                } else if (mode == "EnemyWall") {
                    HealthBar = EnemyHealthBar;
                    wall = EnemyWall;
                }

                toX = Math.min(wall.x1, wall.x2) + Math.abs(wall.x1 - wall.x2) / 2;
                toY = Math.min(wall.y1, wall.y2) + Math.abs(wall.y1 - wall.y2) / 2;

                var move_card = new TWEEN.Tween(card.position)
                        .to({
                            'x': toX, 
                            'y': toY
                            }, 250)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                move_card.start();

                UpdateHealth(HealthBar, GetHealth(HealthBar) - parseInt(card.attack.text, 10));
                card.health.text = 
                    parseInt(card.health.text, 10) - 10;


                var move_card_back = new TWEEN.Tween(card.position)
                        .to({
                            'x': position_from.x, 
                            'y': position_from.y
                            }, 250)
                        .easing(TWEEN.Easing.Quadratic.InOut);

                setTimeout(function(){move_card_back.start();}, 240);
            }

});
