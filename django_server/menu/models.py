import django.utils 

from django import forms
from django.db import models
from django.contrib.auth.models import User

class Card(models.Model):
    cardID = models.IntegerField(default=0)

    def __str__(self):
        return self.cardID    


class Deck(models.Model):
    cards = models.ManyToManyField(Card)
    user = models.ForeignKey(User)

    def __str__(self):
        return self.cards

class EmailForm(forms.Form):
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255)
    contact_email = forms.EmailField()
    subject = forms.CharField(max_length=255)
#    botcheck = forms.CharField(max_length=5)
    message = forms.CharField()

class Battle(models.Model):
    created = models.DateField()
    player1 = models.ForeignKey(User, related_name='player1', null=True)
    player2 = models.ForeignKey(User, related_name='player2', null=True)
    secret_code1 = models.CharField(max_length=60, null=True)
    secret_code2 = models.CharField(max_length=60, null=True)

